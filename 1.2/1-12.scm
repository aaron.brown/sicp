(define (pascal row column)
  (cond 
    ((or
      (= 0 row) 
      (= 0 column) 
      (= row column))
    1
    )
    ((or
      (< row 0)
      (< column 0)
      (< row column)
    )
    (display "\nInput error")
    )
    (else (+ (pascal (- row 1) column) (pascal (- row 1) (- column 1))))
  )
)



(pascal 5 3)
(+ (pascal 4 3) (pascal 4 2))
(+ (+ (pascal 3 3) (pascal )) )

(pascal 5 2)