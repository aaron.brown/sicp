(define (inc n)
  (+ n 1))
(define (dec n)
  (- n 1))
(define (plus a b)
  (if (= a 0)
      b
      (inc (plus (dec a) b))))
(define (plus-iter  a b)
  (if (= a 0)
      b
      (plus-iter (dec a) (inc b))))

