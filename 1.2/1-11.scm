(define (f-recur n)
  (if (< n 3)
    n
    (+ 
      (f-recur (- n 1))
      (* 2 (f-recur (- n 2)))
      (* 3 (f-recur (- n 3)))
    )
  )
)

(define (f-iter n) 
  (define (f-iter-compute a b c)
    (+ c (* 2 b) (* 3 a))
  )
  (define (f-iter-loop a b c count)
    (if (= count n)
      c
      (f-iter-loop b c (f-iter-compute a b c) (+ count 1))
    )
  )
  (if (< n 3)
    n
    (f-iter-loop 0 1 2 2)
  )
)

