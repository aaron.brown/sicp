(define (cbrt x)
  (define (average a b) (/ (+ a b) 2))
  (define (good-enough? guess prev-guess)
    (< (abs (- guess prev-guess)) (/ guess 100000))
  )
  (define (improve guess)
    (/ 
      (+
        (/ x (* guess guess))
        (* guess 2)
      )
      3
    )
  )
  (define (cbrt-iter guess prev-guess)
    (if (good-enough? guess prev-guess)
      guess
      (cbrt-iter (improve guess) guess)
    )
  )
  (cbrt-iter 1.0 0.0)
)

(cbrt 0.001)